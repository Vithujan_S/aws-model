import time
from absl import app, logging
import numpy as np
import tensorflow as tf
import cv2 as cv
from math import atan2, cos, sin, sqrt, pi
import matplotlib.pyplot as plt
from PIL import Image
from flask import Flask, request, Response, jsonify, send_from_directory, abort
import os
import math
from scipy import ndimage

#load weights
net = cv.dnn.readNet('./weights/yolov4-obj_last.weights', './weights/yolov4-obj.cfg')

# load classes
classes = []
with open('./weights/obj.names','r') as f:
    classes = f.read().splitlines()


# draw axiese
def drawAxis(img, p_ , q_ , color, scale):
    p = list(p_)
    q = list(q_)
    
    ## [visualization1]
    angle = atan2(p[1] - q[1], p[0] - q[0]) # angle in radians
    hypotenuse = sqrt((p[1] - q[1]) * (p[1] - q[1]) + (p[0] - q[0]) * (p[0] - q[0]))
 
    # Here we lengthen the arrow by a factor of scale
    q[0] = p[0] - scale * hypotenuse * cos(angle)
    q[1] = p[1] - scale * hypotenuse * sin(angle)
    cv.line(img, (int(p[0]), int(p[1])), (int(q[0]), int(q[1])), color, 3, cv.LINE_AA)
 
    # create the arrow hooks
    p[0] = q[0] + 9 * cos(angle + pi / 4)
    p[1] = q[1] + 9 * sin(angle + pi / 4)
    cv.line(img, (int(p[0]), int(p[1])), (int(q[0]), int(q[1])), color, 3, cv.LINE_AA)
 
    p[0] = q[0] + 9 * cos(angle - pi / 4)
    p[1] = q[1] + 9 * sin(angle - pi / 4)
    cv.line(img, (int(p[0]), int(p[1])), (int(q[0]), int(q[1])), color, 3, cv.LINE_AA)
    ## [visualization1]

# draw orientation
def getOrientation(pts, img):
    ## [pca]
    # Construct a buffer used by the pca analysis
    sz = len(pts)
    data_pts = np.empty((sz, 2), dtype=np.float64)
    for i in range(data_pts.shape[0]):
        data_pts[i,0] = pts[i,0,0]
        data_pts[i,1] = pts[i,0,1]

    # Perform PCA analysis
    mean = np.empty((0))
    
    mean, eigenvectors, eigenvalues = cv.PCACompute2(data_pts, mean)

    # Store the center of the object
    cntr = (int(mean[0,0]), int(mean[0,1]))
    ## [pca]

    ## [visualization]
    # Draw the principal components
    cv.circle(img, cntr, 3, (255, 0, 255), 2)
    p1 = (cntr[0] + 0.02 * eigenvectors[0,0] * eigenvalues[0,0], cntr[1] + 0.02 * eigenvectors[0,1] * eigenvalues[0,0])
    p2 = (cntr[0] - 0.02 * eigenvectors[1,0] * eigenvalues[1,0], cntr[1] - 0.02 * eigenvectors[1,1] * eigenvalues[1,0])
    drawAxis(img, cntr, p1, (255, 255, 0), 1)
    drawAxis(img, cntr, p2, (0, 0, 255), 5)

    angle = atan2(eigenvectors[0,1], eigenvectors[0,0]) # orientation in radians
    ## [visualization]

    # Label with the rotation angle
    #label = "  Rotation Angle: " + str(-int(np.rad2deg(angle)) - 90) + " degrees"
    #textbox = cv.rectangle(img, (cntr[0], cntr[1]-25), (cntr[0] + 250, cntr[1] + 10), (255,255,255), -1)
    #cv.putText(img, label, (cntr[0], cntr[1]), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 1, cv.LINE_AA)

    return int(np.rad2deg(angle))

# detecting angle
def detectAngle(img):

    # Convert image to grayscale
    #gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    blurred_frame = np.array(cv.GaussianBlur(img, (3,3), 0))
    img1 = cv.cvtColor(blurred_frame, cv.COLOR_RGB2BGR)
    #cv.imshow('Input Image', img1)

    lower_blue = np.array([4, 95, 182])
    upper_blue = np.array([179, 255, 255])
    hsv = cv.cvtColor(img1, cv.COLOR_BGR2HSV)
    mask = cv.inRange(hsv, lower_blue, upper_blue)

    kernel = np.ones((4,4),np.uint8)
    dilate = cv.dilate(mask, kernel, iterations=1)
    #img_edges = cv.Canny(dilate, 120, 255, L2gradient = True)

    # Convert image to binary
    _, bw = cv.threshold(dilate, 127, 255, cv.THRESH_BINARY+cv.THRESH_OTSU)
    #bw = cv.adaptiveThreshold(dilate,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C,cv.THRESH_BINARY,11,2)

    # Find all the contours in the thresholded image
    contours, _ = cv.findContours(bw, cv.RETR_LIST, cv.CHAIN_APPROX_NONE)
    c = ''
    for i, c in enumerate(contours):

      # Calculate the area of each contour
        area = cv.contourArea(c)

      # Ignore contours that are too small or too large
        if area < 5000 or 20000 < area:
            continue

      # Draw each contour only for visualisation purposes
        cv.drawContours(img, contours, i, (0, 0, 255), 2)

      # Find the orientation of each shape
        getOrientation(c, img)

    return abs(getOrientation(c, img))



# Initialize Flask application
application = Flask(__name__)

# API that returns JSON with classes found in images
@application.route('/detections', methods=['POST'])
def get_detections():
    raw_images = []
    images = request.files.getlist("images")
    image_names = []
    for image in images:
        image_name = image.filename
        image_names.append(image_name)
        image.save(os.path.join(os.getcwd(), image_name))
        img_raw = cv.imread(image_name)
        raw_images.append(img_raw)
        
    num = 0
    
    # create list for final response
    response = []

    for j in range(len(raw_images)):
        # create list of responses for current image
        responses = []
        raw_img = raw_images[j]
        num+=1
        height, width, _ = raw_img.shape
        obj_angle = 0
        blob = cv.dnn.blobFromImage(raw_img, 1/255, (416,416), (0,0,0), swapRB = True, crop = False)
        net.setInput(blob) # settnig the input to the network
        output_layers_names = net.getUnconnectedOutLayersNames() # Get the output layers name
        layerOutputs = net.forward(output_layers_names) # forward pass and get the out from fthe output layer

        boxes = [] # bounding boxes
        confidences = [] # sotring the confidence
        class_ids = [] # predicted classes

        for output in layerOutputs: # extract all the info from the layerOutputs
            for detection in output: # extract info from each of the output
                scores = detection[5:] # extract all the classes starting from the 6 element because the first 5 elements have the sizes and the coordinates of the bounding box
                class_id = np.argmax(scores) # extract highest score location
                confidence = scores[class_id] # extracting the highest score and assigning to confidence

                if confidence > 0.2: #threash hold 
                    center_x = int(detection[0]*width)
                    center_y = int(detection[1]*height)
                    w = int(detection[2]*width)
                    h = int(detection[3]*height)

                    x = int(center_x - w/2)
                    y = int(center_y - w/2) # getting the possitions upper left corner

                    boxes.append([x,y,w,h])
                    confidences.append((float(confidence)))
                    class_ids.append(class_id)

        indexes = cv.dnn.NMSBoxes(boxes, confidences, 0.3, 0.4) # 0.4 maximum supressions/ NMS - Only getting the highest accuracy boxes
        #font = cv.FONT_HERSHEY_PLAIN
        #colours = np.random.uniform(0, 255, size=(len(boxes), 3))
        t1 = time.time()
        if len(indexes) > 0:
            for i in indexes.flatten():
                #x, y, w, h = boxes[i]
                label = str(classes[class_ids[i]])
                confidence = str(round(confidences[i], 2))
                #color = colours[i]
                #cor = raw_img[y:y+h, x:x+w, :]
                #obj_angle = detectAngle(cor)
        t2 = time.time()
        print('time: {}'.format(t2 - t1))
        '''if obj_angle > 90:
            obj_angle = 180 - obj_angle
        if obj_angle in range(60,65):
            label = "pass"
        else:
            label = "notPass"'''
        
        print('detections:')
        print('\t{}, {}'.format(label,confidence))
        responses.append({
            "class": label,
            "confidence": float("{}".format(confidence))
            #"angle": obj_angle
        })
        response.append({
            "image": image_names[0],
            "detections": responses
        })

    #remove temporary images
    for name in image_names:
        os.remove(name)
    try:
        return jsonify({"response":response}), 200
    except FileNotFoundError:
        abort(404)

# API that returns image with detections on it
'''@app.route('/image', methods= ['POST'])
def get_image():
    image = request.files["images"]
    image_name = image.filename
    image.save(os.path.join(os.getcwd(), image_name))
    img_raw = tf.image.decode_image(
        open(image_name, 'rb').read(), channels=3)
    img = tf.expand_dims(img_raw, 0)
    img = transform_images(img, size)

    t1 = time.time()
    boxes, scores, classes, nums = yolo(img)
    t2 = time.time()
    print('time: {}'.format(t2 - t1))

    print('detections:')
    for i in range(nums[0]):
        print('\t{}, {}, {}'.format(class_names[int(classes[0][i])],
                                        np.array(scores[0][i]),
                                        np.array(boxes[0][i])))
    img = cv2.cvtColor(img_raw.numpy(), cv2.COLOR_RGB2BGR)
    img = draw_outputs(img, (boxes, scores, classes, nums), class_names)
    cv2.imwrite(output_path + 'detection.jpg', img)
    print('output saved to: {}'.format(output_path + 'detection.jpg'))
    
    # prepare image for response
    _, img_encoded = cv2.imencode('.png', img)
    response = img_encoded.tostring()
    
    #remove temporary image
    os.remove(image_name)

    try:
        return Response(response=response, status=200, mimetype='image/png')
    except FileNotFoundError:
        abort(404)'''

if __name__ == '__main__':
    application.run(debug=False, port=5000)